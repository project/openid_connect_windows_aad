# OpenID Connect Microsoft Azure Active Directory client

## Contents of this file

- Introduction
- Requirements
- Installation
- Configuration

## Introduction

The module OpenID Connect Microsoft Azure Active Directory client is a CTools
plugin for the OpenID Connect module and allows integration with Windows
Azure AD.

[Read the module's project page](https://www.drupal.org/project/openid_connect_windows_aad)
for an overview of the features.

## Requirements

This module requires:
- [OpenID Connect / OAuth client module](https://www.drupal.org/project/openid_connect)
- [Key module](https://www.drupal.org/project/key)
- [JSON Web Token library](https://github.com/lcobucci/jwt)

## Installation

Install as you would [normally install a contributed Drupal module](https://www.drupal.org/node/1897420).

## Configuration

- Visit admin/config/services/openid-connect
- Click "+ Windows Azure ID" to add an new OpenID Connect client

Further configuration depends on how Azure has been set up. [Extended
documentation](https://www.drupal.org/node/3152425) is available on
drupal.org:

- [Setting up Azure](https://www.drupal.org/node/3152435)
- [Configuring the Windows Azure client](https://www.drupal.org/node/3152499)
- [Creating a secret with the Key module](https://www.drupal.org/project/key)
